module.exports = (robot) => {
  robot.respond(/(check)/gi, (res) => {
        const http = require('http')
        
        const options = {
               hostname: 'rocketchat',
                port: 3000,
                path: '/api/v1/info',
                method: 'GET',
        };

        const req = http.request(options, result => {
          res.reply(`Hello! Its works!` ) 
          console.log(`statusCode: ${result.statusCode}`);

          result.on('data', d => {
          	const util = require('util');
            res.reply(`${util.inspect(d)}`);
         	console.log(`reply: ${d}`);
        	});
        })

        req.on('error', error => {
        	res.reply(`Hello! Sadly dont work :( `)
          	console.error(error)
        })

        req.end()
    }  
  )  
}	