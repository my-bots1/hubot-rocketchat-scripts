// Description:
//   Get presence in channel in Rocket.Chat
//
// Dependencies:
//   hubot-rocketchat
//
// Configuration:
//   You need create 
//   My account +> Personal Access Tokens +> Ignore Two Factor Authentication 
//   => Create token and User Id => Copy Token and user id and add here to the header
//
// Commands:
//   bot presence - < Tells the status of users on the channel >
//
// Notes:
//   Version 0.1
//
// Author:
//   Pablo Mestre <pmdcuba@gmail.com>
module.exports = (robot) => {                                                   
  robot.respond(/(presence)/gi, (res) => {
      const http = require('http');

      const options = {
        hostname: 'rocketchat',
        port: 3000,
        path: '/api/v1/users.presence',
        method: 'GET',
        headers: {
          "X-Auth-Token": "(ADD TOKEN HERE))",
          "X-User-Id": "ADD USER ID HERE"
        }
      };

      const req = http.request(options, (result) => {
        res.reply(`Hello! Its works!` ) 
        console.log(`STATUS: ${result.statusCode}`);
        console.log(`HEADERS: ${JSON.stringify(result.headers)}`);
        result.setEncoding('utf8');
        result.on('data', (chunk) => {
          console.log(`BODY: ${chunk}`);
          const util = require('util');
          res.reply(`${util.inspect(chunk)}`);         
        });
        result.on('end', () => {
          console.log('No more data in response.');
        });
      });

      req.on('error', (e) => {
        res.reply(`Hello! Sadly dont work :( `)
        console.error(`problem with request: ${e.message}`);
      });

      req.end();
    }  
  )  
} 

