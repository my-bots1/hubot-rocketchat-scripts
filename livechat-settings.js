// Description:
//   Get presence in channel in Rocket.Chat
//
// Dependencies:
//   hubot-rocketchat
//
// Configuration:
//   None
//
// Commands:
//   livechat settings - Retrieves a list of integration settings.
//
// Notes:
//   Version 0.1
//
// Author:
//   Pablo Mestre <pmdcuba@gmail.com>
module.exports = (robot) => {                                                   
  robot.respond(/(livechat settings)/gi, (res) => {
      const http = require('http');

      const options = {
        hostname: 'rocketchat',
        port: 3000,
        path: '/api/v1/livechat/integrations.settings',
        method: 'GET',
        headers: {
          "X-Auth-Token": "(ADD TOKEN HERE))",
          "X-User-Id": "ADD USER ID HERE"
        }
      };

      const req = http.request(options, (result) => {
        res.reply(`Hello! Its works!` ) 
        console.log(`STATUS: ${result.statusCode}`);
        console.log(`HEADERS: ${JSON.stringify(result.headers)}`);
        result.setEncoding('utf8');
        result.on('data', (chunk) => {
          console.log(`BODY: ${chunk}`);
          const util = require('util');
          res.reply(`${util.inspect(chunk)}`);         
        });
        result.on('end', () => {
          console.log('No more data in response.');
        });
      });

      req.on('error', (e) => {
        res.reply(`Hello! Sadly dont work :( `)
        console.error(`problem with request: ${e.message}`);
      });

      req.end();
    }  
  )  
}